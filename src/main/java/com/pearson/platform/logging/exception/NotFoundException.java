package com.pearson.platform.logging.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException(Exception ex) {
        super(ex);
    }
}

