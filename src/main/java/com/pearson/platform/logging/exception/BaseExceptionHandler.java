package com.pearson.platform.logging.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class BaseExceptionHandler {

    @ExceptionHandler(value = GenericException.class)
    public ResponseEntity<CustomErrorResponse> handleGenericException(GenericException e) {
        CustomErrorResponse error = new CustomErrorResponse(e.getErrorCode(), e.getMessage());
        error.setTimestamp(LocalDateTime.now());
        error.setStatus(e.getHttpStatus().value());
        return new ResponseEntity<>(error, e.getHttpStatus());
    }
}
