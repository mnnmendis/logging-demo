package com.pearson.platform.logging.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
@Getter
@Setter
public class GenericException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private int errorCode;
    private String message;
    private HttpStatus httpStatus;

    public GenericException(int errorCode,String msg,HttpStatus status) {
        this.errorCode = errorCode;
        this.message = msg;
        this.httpStatus = status;
    }

    public GenericException(String msg,HttpStatus status) {
        this.message = msg;
        this.httpStatus = status;
    }

    public GenericException(HttpStatus status) {
        this.httpStatus = status;
    }
}

