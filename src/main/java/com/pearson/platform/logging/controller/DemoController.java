package com.pearson.platform.logging.controller;

import com.pearson.platform.logging.exception.CustomErrorResponse;
import com.pearson.platform.logging.exception.GenericException;
import com.pearson.platform.logging.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DemoController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/not/{id}")
    @PreAuthorize("isFullyAuthenticated()")
    public String printMessage(@PathVariable int id) throws NotFoundException{
        log.debug("debug log- id is {}",id);
        log.warn("warn log- id is {}",id);
        log.info("info log- id is {}",id);
        log.error("error log- id is {}",id);
        log.debug("good comment 6");
        if(id <5 ){
            throw new NotFoundException("Id cannot be less than 5");
        }
        return "id is " + id;
    }

    @GetMapping("/bad/{id}")
    public String printMessage2(@PathVariable int id) throws GenericException {
        logger.debug("debug log- id is {}",id);
        logger.warn("warn log- id is {}",id);
        logger.info("info log- id is {}",id);
        logger.error("error log- id is {}",id);
        log.debug("good comment 7- test branch");
        if(id <5 ){
            throw new GenericException(1001,"Id cannot be less than 5", HttpStatus.BAD_REQUEST);
        }
        return "id is " + id;
    }

    @GetMapping("/auth/{id}")
    public String printMessage3(@PathVariable int id) throws GenericException {
        CustomErrorResponse re = null;
        logger.debug("debug log- id is {}", id);
        logger.warn("warn log- id is {}", id);
        logger.info("info log- id is {}", id);
        logger.error("error log- id is {}", id);
        if (id < 5) {
            throw new GenericException(HttpStatus.BAD_REQUEST);
        } else if(id==5){
            throw new GenericException("error",HttpStatus.BAD_REQUEST);
        }else{
            try {
                return re.getErrorMessage();
            }catch (Exception e){
                throw new GenericException("Internal Error",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
